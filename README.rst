==========================================
GASP (Graphics API for Students of Python)
==========================================

A library built on Cairo Graphics that enables absolute beginners to write
1980's style arcade games as an introduction to python.

Homepage: http://launchpad.net/gasp-code

Help: https://answers.launchpad.net/gasp-code

FAQ: https://answers.launchpad.net/gasp-code/+faqs

Bugs/Patches Welcome: http://bugs.launchpad.net/gasp-code/+filebug

There is an excellent coursebook <http://openbookproject.net/pybiblio/gasp/>
which goes over learning to use GASP in your own applications, which is
designed to supplement (but can be used apart from)
<http://openbookproject.net/thinkCSpy/>, a full-fledged introduction to the
Python programming language.

DEPENDENCIES
============

GASP requires the following to function:

* cairo (libcairo2)
* pygobject (libgirepository1.0-dev)
* gtk-3.0 (gir1.2-gtk-3.0)

GASP is tested with Python 3.6


INSTALLING FROM DEBS ON DEBIAN AND UBUNTU 
=========================================

To install GASP on Debian buster:

#. Edit ``/etc/ap/sources.list`` and add the following::

       deb https://mjsir911.gitlab.io/gasp/debian buster main

#. Add the gpg key for this repository with::

       $ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 814C92678D24FC4AA24366ED4CDC0A30858D94F0

#. Run::

       $ sudo apt update; apt install python3-gasp

Instructions are the same for Ubuntu 18.04 (bionic), except replace "buster"
with "bionic" in the ``sources.list`` file.


INSTALLING FROM SOURCE
======================

NOTE: It may be easier to use a pre-built binary for your platform, if it
exists. Please see https://launchpad.net/gasp-code/+download for more details.

In order to install GASP, you need to have setuptools installed.

Please see http://pypi.python.org/pypi/setuptools for instructions on
installing setuptools for your platform.

Then, run:

::

        python gasp/setup.py install

Or, using easy_install:

::

        pip install .


COPYRIGHT
=========

This package is Copyright (c) 2009 the GASP Development Team, as detailed in
the AUTHORS file shipped with this package.

This package is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version. See the COPYING file for more details.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
