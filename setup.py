#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This program is part of GASP, a toolkit for newbie Python Programmers.
# Copyright (C) 2009, the GASP Development Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Installer for the GASP Core Software."""

from setuptools import setup

setup(
    name='gasp',
    version='0.3.7',
    packages=['gasp'],
    author='Jamie Boisture and James Hancock',
    author_email='jamieboisture@gmail.com, jlhancock@gmail.com',
    maintainer='Marco Sirabella',
    maintainer_email='marco@sirabella.org',
    description='Simple, procedural graphics API for beginning students using '
                'Python',
    long_description="""procedural Python graphics library for beginning programmers GASP is a
wrapper around the Python Cairo Graphics library which makes writing graphical
applications in Python easy for beginners, requiring little prior knowledge of
object-oriented programming.""",
    license='GPLv3+',
    keywords='gasp',
    url='https://gitlab.com/mjsir911/gasp',
    download_url='https://gitlab.com/mjsir911/gasp/releases',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: X11 Applications :: GTK',
        'Environment :: Win32 (MS Windows)',
        'Intended Audience :: Education',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Programming Language :: Python',
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: Implementation :: CPython",
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Topic :: Education :: Computer Aided Instruction (CAI)',
        'Topic :: Multimedia :: Graphics',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Software Development :: User Interfaces'
    ],
    package_dir={'gasp': 'gasp'},
    package_data={'gasp': ['images/gasp.png']},
    install_requires=[
        'pycairo >= 1.4',
        'pygobject',
    ],
    extra_require={
        'test': ['nose'],
    },
    zip_safe=False,
)
